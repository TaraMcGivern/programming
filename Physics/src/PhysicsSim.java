
public class PhysicsSim 
{
	private static double[] gravity = {0, 0, 9.81};
	private static double[] k = {0, 0, -1};
	
	public static void SimOne(double objMass, double muS, double muK,
			double[] nBar) 
	{
				double nSquared = 0;
				
				double[] nHat = new double[3];
				
				
				//NBAR AND NHAT
				
				for(int i = 0; i < 3; i++)
					nSquared = nSquared + Math.pow(nBar[i], 2);

				
				for(int i = 0; i < 3; i++)
				{
					nHat[i] = ((1*nBar[i]) / (Math.sqrt(nSquared)));
					//System.out.println(nHat[i]);
				}
				 
				//CALCULATING FORCE OF GRAVITY
				double forceOfGravity[] = PhysicsUtility.getForceOfGravity(gravity, k, objMass);

				//CALCULATING FGN
				double num = PhysicsUtility.getVectorDotProduct(forceOfGravity, nHat);

				double forceOfGN[] = PhysicsUtility.vectorByDot(num, nHat);

				
				//CALCULATING FGP
				double forceOfGP[] = PhysicsUtility.vectorSubtraction(forceOfGravity, forceOfGN);
				
				
				//length of FGP
				num = 0; 
				
				num = PhysicsUtility.getVectorDotProduct(forceOfGP, forceOfGP);
				
				double forceofGP = Math.sqrt(num);
				
				
				//CALCULATING FN
				double forceOfFN[] = PhysicsUtility.getOpposite(forceOfGN);
				double forceofFN = 0; 
				num = 0; 

				num = PhysicsUtility.getVectorDotProduct(forceOfGN, forceOfGN);
				
				forceofFN = PhysicsUtility.getSqrt(num);
				
				//CALCULATE STATIC FRICTION
				double fMax = muS * forceofFN;
				
				
				if(fMax <= forceofGP)
				{
					System.out.println("This object is kinectic");
					double[] kinecticFric = PhysicsUtility.getKineticFriction(forceOfGP, muK, forceofFN);
					//NOTE: Vector is appearing as opposite, signs are switched, will resolve later
					kinecticFric = PhysicsUtility.getOpposite(kinecticFric);
					MainApp.printVector(kinecticFric);
					
					//CALCULATE FNET
					double[] Fnet = PhysicsUtility.vectorAddition(forceOfGP, kinecticFric);
					System.out.println("-------------------");
					MainApp.printVector(Fnet);
					
					//CALCULATE ACCELERATION
					double[] a = PhysicsUtility.getAccerleration(objMass, Fnet);
					System.out.println("-------------------");
					MainApp.printVector(a);
				}
				else
				{
					System.out.println("This object is static.");
					System.out.println(fMax + " " + forceofGP);
				}
		
	}

	public static void simulationTwo(double objMass1, double muS1, double muK1,
			double objMass2, double muS2, double muK2, double[] nHat) 
	{
		//CALCULATING FORCE OF GRAVITY
				double forceOfGravity1[] = PhysicsUtility.getForceOfGravity(gravity, k, objMass1);
				double forceOfGravity2[] = PhysicsUtility.getForceOfGravity(gravity, k, objMass2);

				//CALCULATING FGN
				double num1 = PhysicsUtility.getVectorDotProduct(forceOfGravity1, nHat);
				double num2 = PhysicsUtility.getVectorDotProduct(forceOfGravity2, nHat);

				double forceOfGN1[] = PhysicsUtility.vectorByDot(num1, nHat);
				double forceOfGN2[] = PhysicsUtility.vectorByDot(num2, nHat);
				
				MainApp.printVector(forceOfGN1);
				MainApp.printVector(forceOfGN2);

						
				//CALCULATING FGP
				double forceOfGP1[] = PhysicsUtility.vectorSubtraction(forceOfGravity1, forceOfGN1);
				double forceOfGP2[] = PhysicsUtility.vectorSubtraction(forceOfGravity2, forceOfGN2);
						
						
				//length of FGP
				num1 = 0;
				num2 = 0;
						
				num1 = PhysicsUtility.getVectorDotProduct(forceOfGP1, forceOfGP1);
				num2 = PhysicsUtility.getVectorDotProduct(forceOfGP2, forceOfGP2);
						
				double forceofGP1 = Math.sqrt(num1);
				double forceofGP2 = Math.sqrt(num2);
				
				System.out.println(forceofGP1);
				System.out.println("----------------------");
				System.out.println(forceofGP2);
				
				//FGP Hat
				double forceOfGPHat[] = new double[3];
				forceOfGPHat = PhysicsUtility.getVectorHat(forceOfGP1);
				
				MainApp.printVector(forceOfGPHat);
				
				double forceOfN1[] = PhysicsUtility.getOpposite(forceOfGN1);
				double forceOfN2[] = PhysicsUtility.getOpposite(forceOfGN2);
				
				double  ff1[] = PhysicsUtility.getVectorFriction(muS1, PhysicsUtility.getVectorLength(forceOfN1), PhysicsUtility.getOpposite(forceOfGPHat));
				double  ff2[] = PhysicsUtility.getVectorFriction(muS2, PhysicsUtility.getVectorLength(forceOfN2), PhysicsUtility.getOpposite(forceOfGPHat));
				
				
				// we don't know what T is, keep it at 1 for now 
				//and substitute it later
				double T = 1; 
				
				double fT1[] = PhysicsUtility.vectorByDot(T, forceOfGPHat);
				double fT2[] = PhysicsUtility.vectorByDot(T * -1, forceOfGPHat);
				
				double[] Fnet1 = new double[3];
				double[] Fnet2 = new double[3];
				
				double a = 0; 
				double b = 0;
				
				Fnet1 = PhysicsUtility.vectorAddition(forceOfGravity1, PhysicsUtility.vectorAddition(forceOfN1, ff1));
				Fnet2 = PhysicsUtility.vectorAddition(forceOfGravity2, PhysicsUtility.vectorAddition(forceOfN2, ff2));
				
					/*a = forceofGP1 + PhysicsUtility.getVectorLength(forceOfGN1) +  PhysicsUtility.getVectorLength(forceOfN1) +
							 PhysicsUtility.getVectorLength(ff1);
					b = forceofGP2 + PhysicsUtility.getVectorLength(forceOfGN2) +  PhysicsUtility.getVectorLength(forceOfN2) +
							 PhysicsUtility.getVectorLength(ff2);*/
				double[] Fnet = PhysicsUtility.vectorAddition(Fnet1, Fnet2);

				System.out.println(PhysicsUtility.getVectorLength(Fnet));
				
				double c = PhysicsUtility.getVectorLength(Fnet);
				
				if (c > 0)
				{
					System.out.println("Objects are kinetic.");
					double ff1k[] = PhysicsUtility.getVectorFriction(muK1, PhysicsUtility.getVectorLength(forceOfN1), PhysicsUtility.getOpposite(forceOfGPHat));
					double ff2k[] = PhysicsUtility.getVectorFriction(muK2, PhysicsUtility.getVectorLength(forceOfN2), PhysicsUtility.getOpposite(forceOfGPHat));
					
					Fnet1 = PhysicsUtility.vectorAddition(forceOfGravity1, PhysicsUtility.vectorAddition(forceOfN1, ff1k));
					Fnet2 = PhysicsUtility.vectorAddition(forceOfGravity2, PhysicsUtility.vectorAddition(forceOfN2, ff2k));
					
					double[] FnetK = PhysicsUtility.vectorMultiplication((PhysicsUtility.vectorAddition(Fnet1, Fnet2)),forceOfGPHat);
					
					double FnetKLen = PhysicsUtility.getVectorLength(FnetK);
					
					double[] acc = new double[3];
					
					for (int i = 0; i < 3; i++)
					{
						acc[i] = (1/(objMass1 + objMass2)) * FnetK[i];
					}
					
					System.out.println("Accelation------------------");
					MainApp.printVector(acc);
					
					System.out.println("----------------------Euler's Method--------------------------");
					
					double[] pos = {0, 0, 0};
					double[] vel = {1, 0, 0};
					
					PhysicsUtility.eulersMethod(4, pos, vel, 5, 0, 0.1);
		
	}
	}
}
