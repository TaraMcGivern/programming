import java.util.*;

public class MainApp 
{
	private double[] gravity = {0, 0, 9.81};
	private double[] k = {0, 0, -1};
	
	public static void main(String args[])
	{
		MainApp theApp = new MainApp();
		theApp.start();
	}
	
	private void start()
	{
		Scanner kb = new Scanner(System.in);
		
		System.out.println("Would you like to input varaibles?");
		System.out.println("1.  Yes");
		System.out.println("2.  No");
		String choice = kb.nextLine();
		
		if(choice.equals("1"))
		{
			System.out.println("\n Choose a simulation \n");
			System.out.println("1. Single object");
			System.out.println("2. Two objects on plane connected by massless rod");
			String choice1 = kb.nextLine();
			if(choice1.equals("1"))
			{
				System.out.println("\nPlease enter a mass of the object");
				double mass = kb.nextDouble();
				System.out.println("Please enter a mu STATIC of the object");
				double muS =  kb.nextDouble();
				System.out.println("Please enter a mu KINETIC of the object");
				double muK = kb.nextDouble();
				System.out.println("Please enter the normal. Hit Enter after each number. ");
				double a = kb.nextDouble();
				double b = kb.nextDouble();
				double c = kb.nextDouble();
				double[] normal = {a, b, c};
				
				System.out.println("\n\nSimulation One");
				System.out.println("----------------------------");
				PhysicsSim.SimOne(mass, muS, muK, normal);
			}
			else if(choice1.equals("2"))
			{
				System.out.println("\nPlease enter a mass of the object 1");
				double mass1 = kb.nextDouble();
				System.out.println("Please enter a mu STATIC of the object 1");
				double muS1 =  kb.nextDouble();
				System.out.println("Please enter a mu KINETIC of the object 1");
				double muK1 = kb.nextDouble();
				System.out.println("Please enter the normal. Hit Enter after each number. ");
				double a = kb.nextDouble();
				double b = kb.nextDouble();
				double c = kb.nextDouble();
				double[] normal = {a, b, c};
				System.out.println("\nPlease enter a mass of the object 2");
				double mass2 = kb.nextDouble();
				System.out.println("Please enter a mu STATIC of the object 2");
				double muS2 =  kb.nextDouble();
				System.out.println("Please enter a mu KINETIC of the object 2");
				double muK2 = kb.nextDouble();
				System.out.println("\n\nSimulation Two");
				System.out.println("----------------------------");
				PhysicsSim.simulationTwo(mass1, muS1, muK1, mass2, muS2, muK2, normal);
			}
		}
		else
		{
			System.out.println("\n Choose a simulation \n");
			System.out.println("1. Single static object");
			System.out.println("2. Single Kinectic object");
			System.out.println("3. Two objects on plane connected by massless rod");
			String choice1 = kb.nextLine();
			if(choice1.equals("1"))
			{
				System.out.println("\n\nSimulation One");
				System.out.println("----------------------------");
				simulationOne();
			}
			else if(choice1.equals("2"))
			{
				System.out.println("\n\nSimulation Two");
				System.out.println("----------------------------");
				simulationTwo();
			}
			else if(choice1.equals("3"))
			{
				System.out.println("\n\nSimulation Three");
				System.out.println("----------------------------");
				simulationThree();
			}
		}
	}
	
	private void simulationOne() 
	{
		//Initialize
		//plane's coordinates
		//Normal
		double[] nBar = new double[]{1, -2, 12};
		//object's starting position
		//object's mass
		double objMass = 0.5;
		//object's initial and current velocity and acceleration
		//mu
		double muS = 0.6;
		double muK = 0.5;
		//friction
		
		double nSquared = 0;
		
		double[] nHat = new double[3];
		
		
		//NBAR AND NHAT
		
		for(int i = 0; i < 3; i++)
			nSquared = nSquared + Math.pow(nBar[i], 2);

		
		for(int i = 0; i < 3; i++)
		{
			nHat[i] = ((1*nBar[i]) / (Math.sqrt(nSquared)));
			//System.out.println(nHat[i]);
		}
		 
		//CALCULATING FORCE OF GRAVITY
		double forceOfGravity[] = PhysicsUtility.getForceOfGravity(gravity, k, objMass);
		System.out.println("Force of Gravity");
		printVector(forceOfGravity);

		//CALCULATING FGN
		double num = PhysicsUtility.getVectorDotProduct(forceOfGravity, nHat);

		double forceOfGN[] = PhysicsUtility.vectorByDot(num, nHat);
		System.out.println("Force of GN");
		printVector(forceOfGN);

		
		//CALCULATING FGP
		double forceOfGP[] = PhysicsUtility.vectorSubtraction(forceOfGravity, forceOfGN);
		System.out.println("Force of GP");
		printVector(forceOfGP);
		
		//length of FGP
		num = 0; 
		
		num = PhysicsUtility.getVectorDotProduct(forceOfGP, forceOfGP);
		
		double forceofGP = Math.sqrt(num);
		
		
		//CALCULATING FN
		double forceOfFN[] = PhysicsUtility.getOpposite(forceOfGN);
		System.out.println("Force of FN");
		printVector(forceOfFN);
		double forceofFN = 0; 
		num = 0; 

		num = PhysicsUtility.getVectorDotProduct(forceOfGN, forceOfGN);
		
		forceofFN = PhysicsUtility.getSqrt(num);
		
		//CALCULATE STATIC FRICTION
		double fMax = muS * forceofFN;
		
		
		if(fMax <= forceofGP)
		{
			System.out.println("This object is kinectic");
			double[] kinecticFric = PhysicsUtility.getKineticFriction(forceOfGP, muK, forceofFN);
			//NOTE: Vector is appearing as opposite, signs are switched, will resolve later
			kinecticFric = PhysicsUtility.getOpposite(kinecticFric);
			printVector(kinecticFric);
			
			//CALCULATE FNET
			double[] Fnet = PhysicsUtility.vectorAddition(forceOfGP, kinecticFric);
			System.out.println("-------------------");
			printVector(Fnet);
			
			//CALCULATE ACCELERATION
			double[] a = PhysicsUtility.getAccerleration(objMass, Fnet);
			System.out.println("-------------------");
			printVector(a);
		}
		else
		{
			System.out.println("This object is static.");
			System.out.println(fMax + " " + forceofGP);
		}
	}
	
	private void simulationTwo()
	{
		//Initialize
		//plane's coordinates
		//Normal
		double[] nBar = new double[]{2, -4, 3};
		//object's starting position
		//object's mass
		double objMass = 0.8;
		//object's initial and current velocity and acceleration
		//mu
		double muS = 0.7;
		double muK = 0.4;
		//friction
		
		double nSquared = 0;
		
		double[] nHat = new double[3];
		
		
		//NBAR AND NHAT
		
		for(int i = 0; i < 3; i++)
			nSquared = nSquared + Math.pow(nBar[i], 2);

		
		for(int i = 0; i < 3; i++)
		{
			nHat[i] = ((1*nBar[i]) / (Math.sqrt(nSquared)));
			//System.out.println(nHat[i]);
		}
		 
		//CALCULATING FORCE OF GRAVITY
		double forceOfGravity[] = PhysicsUtility.getForceOfGravity(gravity, k, objMass);
		System.out.println("Force of Gravity");
		printVector(forceOfGravity);

		//CALCULATING FGN
		double num = PhysicsUtility.getVectorDotProduct(forceOfGravity, nHat);

		double forceOfGN[] = PhysicsUtility.vectorByDot(num, nHat);
		System.out.println("Force of GN");
		printVector(forceOfGN);
		
		//CALCULATING FGP
		double forceOfGP[] = PhysicsUtility.vectorSubtraction(forceOfGravity, forceOfGN);
		System.out.println("Force of GP");
		printVector(forceOfGP);
		
		//length of FGP
		num = 0; 
		
		num = PhysicsUtility.getVectorDotProduct(forceOfGP, forceOfGP);
		
		double forceofGP = Math.sqrt(num);
		
		
		//CALCULATING FN
		double forceOfFN[] = PhysicsUtility.getOpposite(forceOfGN);
		System.out.println("Force of FN");
		printVector(forceOfFN);
		double forceofFN = 0; 
		num = 0; 

		num = PhysicsUtility.getVectorDotProduct(forceOfGN, forceOfGN);
		
		forceofFN = PhysicsUtility.getSqrt(num);
		
		//CALCULATE STATIC FRICTION
		double fMax = muS * forceofFN;
		
		
		if(fMax <= forceofGP)
		{
			System.out.println("This object is kinectic");
			double[] kinecticFric = PhysicsUtility.getKineticFriction(forceOfGP, muK, forceofFN);
			//NOTE: Vector is appearing as opposite, signs are switched, will resolve later
			kinecticFric = PhysicsUtility.getOpposite(kinecticFric);
			printVector(kinecticFric);
			
			//CALCULATE FNET
			double[] Fnet = PhysicsUtility.vectorAddition(forceOfGP, kinecticFric);
			System.out.println("-------------------");
			printVector(Fnet);
			
			//CALCULATE ACCELERATION
			double[] a = PhysicsUtility.getAccerleration(objMass, Fnet);
			System.out.println("-------------------");
			printVector(a);
		}
		else
		{
			System.out.println("This object is static.");
			System.out.println(fMax + " " + forceofGP);
		}
	}
	
	private void simulationThree()
	{
		//Initialize
		//plane's coordinates
		//Normal
		double[] nHat = new double[]{0.28, 0, 0.96};
		//object's starting position
		//object's mass
		double objMass1 = 125/12;
		double objMass2 = 125/24;
		//object's initial and current velocity and acceleration
		//mu
		double muS1 = 0.2;
		double muS2 = 0.3;
		double muK1 = 0.07;
		double muK2 = 0.14;
		//friction
			
				 
		//CALCULATING FORCE OF GRAVITY
		double forceOfGravity1[] = PhysicsUtility.getForceOfGravity(gravity, k, objMass1);
		double forceOfGravity2[] = PhysicsUtility.getForceOfGravity(gravity, k, objMass2);

		//CALCULATING FGN
		double num1 = PhysicsUtility.getVectorDotProduct(forceOfGravity1, nHat);
		double num2 = PhysicsUtility.getVectorDotProduct(forceOfGravity2, nHat);

		double forceOfGN1[] = PhysicsUtility.vectorByDot(num1, nHat);
		double forceOfGN2[] = PhysicsUtility.vectorByDot(num2, nHat);
		
		printVector(forceOfGN1);
		printVector(forceOfGN2);

				
		//CALCULATING FGP
		double forceOfGP1[] = PhysicsUtility.vectorSubtraction(forceOfGravity1, forceOfGN1);
		double forceOfGP2[] = PhysicsUtility.vectorSubtraction(forceOfGravity2, forceOfGN2);
				
				
		//length of FGP
		num1 = 0;
		num2 = 0;
				
		num1 = PhysicsUtility.getVectorDotProduct(forceOfGP1, forceOfGP1);
		num2 = PhysicsUtility.getVectorDotProduct(forceOfGP2, forceOfGP2);
				
		double forceofGP1 = Math.sqrt(num1);
		double forceofGP2 = Math.sqrt(num2);
		
		System.out.println(forceofGP1);
		System.out.println("----------------------");
		System.out.println(forceofGP2);
		
		//FGP Hat
		double forceOfGPHat[] = new double[3];
		forceOfGPHat = PhysicsUtility.getVectorHat(forceOfGP1);
		
		printVector(forceOfGPHat);
		
		double forceOfN1[] = PhysicsUtility.getOpposite(forceOfGN1);
		double forceOfN2[] = PhysicsUtility.getOpposite(forceOfGN2);
		
		double  ff1[] = PhysicsUtility.getVectorFriction(muS1, PhysicsUtility.getVectorLength(forceOfN1), PhysicsUtility.getOpposite(forceOfGPHat));
		double  ff2[] = PhysicsUtility.getVectorFriction(muS2, PhysicsUtility.getVectorLength(forceOfN2), PhysicsUtility.getOpposite(forceOfGPHat));
		
		
		// we don't know what T is, keep it at 1 for now 
		//and substitute it later
		double T = 1; 
		
		double fT1[] = PhysicsUtility.vectorByDot(T, forceOfGPHat);
		double fT2[] = PhysicsUtility.vectorByDot(T * -1, forceOfGPHat);
		
		double[] Fnet1 = new double[3];
		double[] Fnet2 = new double[3];
		
		double a = 0; 
		double b = 0;
		
		Fnet1 = PhysicsUtility.vectorAddition(forceOfGravity1, PhysicsUtility.vectorAddition(forceOfN1, ff1));
		Fnet2 = PhysicsUtility.vectorAddition(forceOfGravity2, PhysicsUtility.vectorAddition(forceOfN2, ff2));
		
			/*a = forceofGP1 + PhysicsUtility.getVectorLength(forceOfGN1) +  PhysicsUtility.getVectorLength(forceOfN1) +
					 PhysicsUtility.getVectorLength(ff1);
			b = forceofGP2 + PhysicsUtility.getVectorLength(forceOfGN2) +  PhysicsUtility.getVectorLength(forceOfN2) +
					 PhysicsUtility.getVectorLength(ff2);*/
		double[] Fnet = PhysicsUtility.vectorAddition(Fnet1, Fnet2);

		System.out.println(PhysicsUtility.getVectorLength(Fnet));
		
		double c = PhysicsUtility.getVectorLength(Fnet);
		
		if (c > 0)
		{
			System.out.println("Objects are kinetic.");
			double ff1k[] = PhysicsUtility.getVectorFriction(muK1, PhysicsUtility.getVectorLength(forceOfN1), PhysicsUtility.getOpposite(forceOfGPHat));
			double ff2k[] = PhysicsUtility.getVectorFriction(muK2, PhysicsUtility.getVectorLength(forceOfN2), PhysicsUtility.getOpposite(forceOfGPHat));
			
			Fnet1 = PhysicsUtility.vectorAddition(forceOfGravity1, PhysicsUtility.vectorAddition(forceOfN1, ff1k));
			Fnet2 = PhysicsUtility.vectorAddition(forceOfGravity2, PhysicsUtility.vectorAddition(forceOfN2, ff2k));
			
			double[] FnetK = PhysicsUtility.vectorMultiplication((PhysicsUtility.vectorAddition(Fnet1, Fnet2)),forceOfGPHat);
			
			double FnetKLen = PhysicsUtility.getVectorLength(FnetK);
			
			double[] acc = new double[3];
			
			for (int i = 0; i < 3; i++)
			{
				acc[i] = (1/(objMass1 + objMass2)) * FnetK[i];
			}
			
			System.out.println("Accelation------------------");
			printVector(acc);
		}
		
		
		
		System.out.println("----------------------Euler's Method--------------------------");
		
		double[] pos = {0, 0, 0};
		double[] vel = {1, 0, 0};
		
		PhysicsUtility.eulersMethod(4, pos, vel, 5, 0, 0.1);
		
	}
	
	public static void printVector(double[] a)
	{
		for(int i = 0; i < 3; i++)
		{
			System.out.println(a[i]);
		}
		System.out.println("-------------------");
	}
	
}