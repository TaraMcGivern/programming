
public class PhysicsUtility 
{
	//Vector sizes are set to three. 
	
	public static double getSqrt(double a)
	{
		return Math.sqrt(a);
	}
	
	//returns the dot product of vectors passed
	public static double getVectorDotProduct(double a[], double b[])
	{
		double result = 0;
		
		for(int i = 0; i < 3; i++)
		{
			result = result + (a[i] * b[i]);
		}

		return result;
	}
	
	//Inverts the signs of the vector
	public static double[] getOpposite(double[] a)
	{
		for(int i = 0; i < 3; i++)
		{
			a[i] = a[i] * -1;
		}
		
		return a;
	}
	
	//vector minus vector
	public static double[] vectorSubtraction(double[] a , double[] b)
	{
		double[] result = new double[3];
		
		for(int i = 0; i < 3; i++)
		{
			result[i] = a[i] - b[i];
		}
		
		return result;
	}
	
	//dot product multiplied by vector
	public static double[] vectorByDot(double a, double[] b)
	{
		double[] result = new double[3];
		
		for(int i = 0; i < 3; i++)
		{
			result[i] = a * b[i];
		}
		
		return result;
	}
	
	
	//gets the force of Gravity
	public static double[] getForceOfGravity(double[] gravity, double[] k, double mass)
	{
		double[] fGravity = new double[3];
		
		for(int i = 0; i < 3; i++)
		{
			fGravity[i] = gravity[i] * k[i] * mass;
		} 
		
		return fGravity;
	}
	
	//gets the hat vector
	public static double[] getVectorHat(double[] a)
	{
		double[] result = new double[3];
		double aSqr = 0; 
		
		for(int i = 0; i < 3; i++)
		{
			aSqr = aSqr + Math.pow(a[i], 2);
		}
		
		aSqr = getSqrt(aSqr);
		
		for(int i = 0; i < 3; i++)
		{
			result[i] = ((1*a[i]) / aSqr);
		}
		
		return result;
	}

	//calculates the vector for kinectic friction
	public static double[] getKineticFriction(double[] a, double b,
			double c) 
	{
		double[] hat = new double[3];
		double Hat  = getVectorDotProduct(a, a);
		Hat = getSqrt(Hat);
		Hat = 1/Hat;
		
		hat = vectorByDot(Hat, a);

		double ff  = b * c;
		
		double[] friction = vectorByDot(ff, hat);
		
		return friction;	
	}

	public static double[] vectorAddition(double[] a, double[] b) 
	{
		double[] c = new double[3];
		
		for(int i = 0; i < 3; i++)
		{
			c[i] = a[i] + b[i];
		}
		
		return c;
	}
	
	public static double[] vectorMultiplication(double[] a, double[] b) 
	{
		double[] c = new double[3];
		
		for(int i = 0; i < 3; i++)
		{
			for(int j = 0; j < 3; j++)
			{
				c[i] = a[i] * b[j];
			}
		}
		
		return c;
	}

	public static double[] getAccerleration(double m, double[] f) 
	{
		double[] a = new double[3];
		
		for(int i = 0; i < 3; i++)
		{
			a[i] = ((1/m) * f[i]);
		}

		return a;
	}
	
	
	public static double[] getVectorFriction(double m, double a, double[] b)
	{
		double[] result = new double[3];
		
		for(int i = 0; i < 3; i++)
		{
			result[i] = m * a * b[i];
		}

		return result;
	}
	
	public static double getVectorLength(double[] a)
	{
		double b = 0;
		
		b = getVectorDotProduct(a, a);
		b = getSqrt(b);
		
		return b;
	}
	
	public static void eulersMethod(int steps, double[] p, double[] v, double speed, double t, double increase)
	{
		double gravity = -9.81f;
		double[] k = {0, 0, 1};
		
		
		double[] a = new double[3];
		
		for(int i = 0; i < 3; i++)
			a[i] = (double)(gravity*k[i] - 0.01*speed*v[i]);
		
		System.out.println("Round " + 0 + ": ");
		System.out.println("T: " + t);
		System.out.println("P: " + p[0] + ", " + p[1] + ", " + p[2]);
		System.out.println("V: " + v[0] + ", " + v[1] + ", " + v[2]);
		System.out.println("A: " + a[0] + ", " + a[1] + ", " + a[2]);
		System.out.println("\n");
		
		for(int i = 1; i < steps; i++)
		{
			t = t + increase;
			
			for(int j = 0; j < 3; j++)
			{
				p[j] = p[j] + increase*v[j];
				
				v[j] = v[j] + increase*a[j];
			}
			
			for(int x = 0; x < 3; x++)
				a[x] = (double)(gravity*k[x] - 0.01*speed*v[x]);
			
			
			
				System.out.println("Round " + i + ": ");
				System.out.println("T: " + t);
				System.out.println("P: " + p[0] + ", " + p[1] + ", " + p[2]);
				System.out.println("V: " + v[0] + ", " + v[1] + ", " + v[2]);
				System.out.println("A: " + a[0] + ", " + a[1] + ", " + a[2]);
				System.out.println("\n");
			
		}
	}
	
}
